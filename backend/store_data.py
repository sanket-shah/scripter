
def parse_data(data):
    '''
    Takes the raw dataframe and parses it according as given in the problem.
    '''
    data = data.rename(columns={"SC_CODE": "code", "SC_NAME": "name", "OPEN": "open", "HIGH": "high", "LOW": "low", "CLOSE": "close"})
    data = data[['code', 'name', 'open', 'high', 'low', 'close']]
    return data


def store_to_redis(conn, data):
    '''
    Takes the parsed data and punches it in Redis.
    '''
    data_d=data.to_dict('records')
    for item in data_d:
        name = item.pop('name')
        name = name.rstrip()
        conn.hmset(name,item)

def read_from_redis(conn):
    '''
    Gets first 10 stocks from Redis.
    '''
    script_list=[]
    for key in conn.keys():
        if len(script_list)<=10:
            result_dict = conn.hgetall(key)
            result_dict = {k: v.decode("utf-8") for k, v in result_dict.items()}
            script_data = [key.decode("utf-8"), result_dict[b'open'], result_dict[b'high'],
                           result_dict[b'low'], result_dict[b'close']]
            script_list.append(script_data)
    return script_list

def search_in_redis(conn,key):
    '''
    Searches the given stock name in redis and returns if anything is found.
    '''
    script_list=[]
    for matching_scipt in conn.scan_iter("*"+key+"*"):
        result_dict = conn.hgetall(matching_scipt)
        result_dict = {k: v.decode("utf-8") for k, v in result_dict.items()}
        script_data = [matching_scipt.decode("utf-8"),result_dict[b'open'],result_dict[b'high'],result_dict[b'low'],result_dict[b'close']]
        script_list.append(script_data)
    return script_list

