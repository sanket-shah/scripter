from bs4 import BeautifulSoup
import requests
import  zipfile, io
import os
import pandas as pd
import configs
data_path = os.path.join(configs.WORKDIR,"data")

def download_bhaavcopy():
    '''
    Scrapes the BSE website and extracts the downloaded zip folder into "data" folder.
    '''
    bse_link = configs.BSE_LINK
    bse_page = requests.get(bse_link).text
    soup = BeautifulSoup(bse_page, 'html.parser')
    bhaavcopy_link = soup.find_all(id=configs.ID)
    bhaavcopy_link = bhaavcopy_link[0]['href']
    download_response = requests.get(bhaavcopy_link)
    z = zipfile.ZipFile(io.BytesIO(download_response.content))
    empty_data_folder(path=data_path)
    z.extractall(path=data_path)

def get_todays_bhaavcopy():
    '''
    Converts latest downloaded bhaavcopy to pandas dataframe.
    '''
    data_list = os.listdir(data_path)
    data = pd.read_csv(os.path.join(data_path,data_list[0]))
    return data


def empty_data_folder(path):
    '''
    Removes any existing histrocal bhaavcopies.
    '''
    folder = path
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

