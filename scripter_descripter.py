import cherrypy
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader('templates'))
from configs import REDIS_HOST,SERVER_HOST,SERVER_PORT
import redis
from backend import get_data,store_data
class Root:
    @cherrypy.expose
    def index(self):
        tmpl = env.get_template('index.html')
        conn = redis.Redis(host=REDIS_HOST)
        get_data.download_bhaavcopy()
        data = get_data.get_todays_bhaavcopy()
        data = store_data.parse_data(data)
        store_data.store_to_redis(conn, data)
        script_list = store_data.read_from_redis(conn)
        return tmpl.render(data=script_list)

    @cherrypy.expose
    def find_script(self,script_name):
        tmp2 = env.get_template('find_scripts.html')
        conn = redis.Redis(host=REDIS_HOST)
        script_name = script_name.upper()
        script_list = store_data.search_in_redis(conn,script_name)
        return tmp2.render(data=script_list)


cherrypy.config.update({'server.socket_host':SERVER_HOST,
                         'server.socket_port': SERVER_PORT,
                        })

cherrypy.quickstart(Root())